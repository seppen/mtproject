##MTProject
Compiles SimpleC3 to MSIL. University related.
The init commit is refactored starter code.

The detailed definition of SimpleC3 is in Simple-C-3.txt
##Examples
Definition of function inside function. (it is supported in every block);

    {
       pchar hello(pchar name) {
          pchar add_signs(int count) {
              pchar signs;
              while(count > 0) {
                  signs += "!";
                  count--;
              }
			  return signs;
          }
          return "hello, " + name + add_signs(5);
       }

	   printf("Enter your name:");
	   printf(hello(scanf_s()));
    }

Check if number is even or odd;

    printf("Example of if statement");
    {
        int a;
        a = scanf_i();
        if (a % 2 == 0) {
            printf("even");
        } else {
            printf("odd");
        }
    }
	