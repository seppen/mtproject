using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using MTProject.Core;
using MTProject.Core.Tokens;

namespace MTProject.Test
{

    public class VerifyDiagnostics : IDiagnostics
    {
        class DiagnosticItem
        {
            int line;
            public int Line
            {
                get { return this.line; }
            }

            string message;
            public string Message
            {
                get { return this.message; }
            }

            public DiagnosticItem(int line, string message)
            {
                this.line = line;
                this.message = message;
            }

            public override string ToString()
            {
                return this.Message + "\t on line: " + this.Line;
            }

        }

        List<DiagnosticItem> seenErrors = new List<DiagnosticItem>();
        List<DiagnosticItem> expectedErrors = new List<DiagnosticItem>();
        string currentSourceFile = null;

        public void Error(int line, int column, string message)
        {
            this.seenErrors.Add(new DiagnosticItem(line, message));
        }

        public void Note(int line, int column, string message)
        {
            throw new NotImplementedException();
        }

        public void Warning(int line, int column, string message)
        {
            throw new NotImplementedException();
        }

        public int GetErrorCount()
        {
            return this.CompareErrorLists();
        }

        public void BeginSourceFile(string sourceFile)
        {
            this.currentSourceFile = sourceFile;
        }

        public void EndSourceFile()
        {
            this.currentSourceFile = null;
        }

        private void GetExpectedErrors()
        {
            StreamReader reader = new StreamReader(currentSourceFile);

            Scanner scanner = new Scanner(reader);
            scanner.SkipComments = false;
            Scanner commentScanner;
            Token t = scanner.Next();

            while (!(t is EOFToken))
            {
                if (t is CommentToken)
                {
                    commentScanner = new Scanner(new StringReader((t as CommentToken).Value));
                    Token errorMessage = commentScanner.Next();
                    do
                    {
                        if ((errorMessage is IdentToken) && (errorMessage as IdentToken).Value == "expectederror")
                            errorMessage = commentScanner.Next();

                        if (errorMessage is StringToken)
                            this.expectedErrors.Add(new DiagnosticItem(t.Line, (errorMessage as StringToken).Value));
                        errorMessage = commentScanner.Next();
                    } while (!(errorMessage is EOFToken));
                }
                t = scanner.Next();
            }

            //Debug.WriteLine("----------------------");
            //DumpSeenErrors();
            //DumpExpectedErrors();
        }

        private void DumpExpectedErrors()
        {
            Console.WriteLine("Errors expected but not seen: " + this.expectedErrors.Count);

            foreach (DiagnosticItem error in expectedErrors)
            {
                Console.WriteLine(error.ToString());
            }
            Console.WriteLine("\n");
        }

        private void DumpSeenErrors()
        {
            Console.WriteLine("Errors seen but not expected: " + this.seenErrors.Count);

            foreach (DiagnosticItem error in this.seenErrors)
            {
                Console.WriteLine(error.ToString());
            }
            Console.WriteLine("\n");
        }

        private int CompareErrorLists()
        {
            this.GetExpectedErrors();
            for (int i = this.seenErrors.Count - 1; i >= 0; --i)
            {
                DiagnosticItem seenError = this.seenErrors[i];
                // Check whether we had more than one error on line. In that case
                // we can neglect the exact ordering.
                for (int j = this.expectedErrors.Count - 1; j >= 0; --j)
                {
                    DiagnosticItem expectedError = this.expectedErrors[j];
                    // If the messages and lines correspond to each other we 
                    // pop the pair (seen-expected) from the lists.
                    if (seenError.Line == expectedError.Line)
                        if (seenError.Message == expectedError.Message)
                        {
                            this.seenErrors.Remove(seenError);
                            this.expectedErrors.Remove(expectedError);
                            // Make sure that if we have repeating expected errors 
                            // we are removing only one of them.
                            break;
                        }
                }
            }
            // Dump the diffs
            if (this.seenErrors.Count > 0)
            {
                this.DumpSeenErrors();
            }

            if (this.expectedErrors.Count > 0)
            {
                this.DumpExpectedErrors();
            }

            return this.seenErrors.Count + this.expectedErrors.Count;
        }
    }
}
