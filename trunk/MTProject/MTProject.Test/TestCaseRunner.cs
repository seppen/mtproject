using System;
using System.Diagnostics;
using System.IO;

using NUnit.Framework;

using MTProject.Core;

namespace MTProject.Test
{
    public abstract class TestCaseRunner
    {
        protected string testCasesDirPath;
        protected string testCasesDirName = "TestCases";
        protected string tmpDirName = "tmp";
        protected string binaryExtension = "exe";
        protected string resultsExtension = "result";

        public TestCaseRunner()
        {
            this.testCasesDirPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", "..", this.Version(), this.testCasesDirName);
        }

        public abstract string Version();
        public abstract string Extension();


        protected static string Normalize(string s)
        {
            char[] charsToTrim = new char[] { ' ', '\t' };
            // for Mac, Win, Lin
            return s.Normalize().Replace("\n\r", "\n").Replace("\r\n", "\n").Replace("\r", "\n").Trim(charsToTrim);
        }

        protected void RunTestMethod(string testname)
        {
            string testPath = Path.Combine(this.testCasesDirPath, testname);
            string testCasesTmpDirPath = Path.Combine(this.testCasesDirPath, this.tmpDirName);
            string sourceFilePath = Path.ChangeExtension(testPath, this.Extension());
            string exeFilePath = Path.ChangeExtension(Path.Combine(testCasesTmpDirPath, testname), this.binaryExtension);
            string resultFilePath = Path.ChangeExtension(testPath, this.resultsExtension);

            // Replace the default diagnostic client with our new custom one.
            // VerifyDiagnostics analyzes special syntax hidden in the comments
            // and verifies whether an error was expected or not.
            bool isCompiled = Compiler.Compile(sourceFilePath, exeFilePath, new VerifyDiagnostics());
            Assert.IsTrue(isCompiled);

            // If the compilation was fine we have an assembly.
            // If there is a testname.result file we run the assembly and 
            // compare the output of the assembly and the expected output
            // in the .result file.
            if (File.Exists(resultFilePath))
            {
                string resultFileText = Normalize(File.ReadAllText(resultFilePath));
                Process p = new Process();
                p.StartInfo.FileName = exeFilePath;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.ErrorDialog = false;

                p.Start();
                if (!p.WaitForExit(15000)) p.Kill();

                string output = Normalize(p.StandardOutput.ReadToEnd());
                string error = Normalize(p.StandardError.ReadToEnd());

                Assert.AreEqual(resultFileText, output, "Mismatch! Output was not expected:\n");
            }
        }
    }
}
