﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NUnit.Framework;

using MTProject.Core;
using MTProject.Core.Tokens;

namespace MTProject.Test.SimpleC3
{

    [TestFixture]
    class ScannerTestFixture
    {
        IScanner scanner;
        TextReader reader;

        [Test]
        public void IfSkipsCommentsTest()
        {
            this.reader = new StringReader("/* 123 */ a");
            this.scanner = new Scanner(this.reader);
            this.scanner.SkipComments = true;
            Assert.IsTrue(this.scanner.Next() is IdentToken);
        }

        [Test]
        public void NumberTokenTest()
        {
            this.reader = new StringReader("123");
            this.scanner = new Scanner(this.reader);
            Assert.IsTrue(this.scanner.Next() is NumberToken);
        }

        [Test]
        [TestCase("if")]
        [TestCase("else")]
        [TestCase("while")]
        [TestCase("break")]
        [TestCase("continue")]
        [TestCase("return")]
        public void KeywordTokenTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Token token = this.scanner.Next();
            Assert.IsTrue(token is KeywordToken && ((KeywordToken)token).Value == input);
        }

        [Test]
        [TestCase("=")]
        [TestCase("(")]
        [TestCase(")")]
        [TestCase(";")]
        [TestCase("+")]
        [TestCase("-")]
        [TestCase("*")]
        [TestCase("|")]
        [TestCase("/")]
        [TestCase("%")]
        [TestCase("&")]
        [TestCase("~")]
        [TestCase(">")]
        [TestCase("<")]
        [TestCase("!")]
        public void SpecialSymbolSingleTokenTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Token token = this.scanner.Next();
            Assert.IsTrue(token is SpecialSymbolToken && ((SpecialSymbolToken)token).Value == input);
        }

        [Test]
        [TestCase("<=")]
        [TestCase("!=")]
        [TestCase(">=")]
        [TestCase("==")]
        [TestCase("||")]
        [TestCase("&&")]
        [TestCase("++")]
        [TestCase("--")]
        [TestCase("+=")]
        [TestCase("-=")]
        [TestCase("*=")]
        [TestCase("/=")]
        [TestCase("%=")]
        public void SpecialSymbolPairsTokenTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Token token = this.scanner.Next();
            Assert.IsTrue(token is SpecialSymbolToken && ((SpecialSymbolToken)token).Value == input);
        }

        [Test]
        [TestCase("a=true", true, true)]
        [TestCase("b=false", true, false)]
        public void BooleanTokenTest(string input, bool isBoolean, bool value)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            this.scanner.Next();
            this.scanner.Next();
            Token token = this.scanner.Next();
            Assert.AreEqual(isBoolean, token is BooleanToken);
            Assert.AreEqual(value, ((BooleanToken)token).Value);
        }

        [Test]
        public void BooleanTokenCannotBeMistakenToStringTest()
        {
            this.reader = new StringReader("a=\"false\"");
            this.scanner = new Scanner(this.reader);
            this.scanner.Next();
            this.scanner.Next();
            Token token = this.scanner.Next();
            Assert.IsFalse(token is BooleanToken);
        }

        [Test]
        [TestCase("x='a'", true, 'a')]
        public void CharTokenTest(string input, bool isChar, char value)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            this.scanner.Next();
            this.scanner.Next();
            Token token = this.scanner.Next();
            Assert.AreEqual(isChar, token is CharToken);
            Assert.AreEqual(value, ((CharToken)token).Value);
        }
    }
}
