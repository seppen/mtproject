﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NUnit.Framework;

using NSubstitute;

using MTProject.Core;

namespace MTProject.Test.SimpleC3
{
    [TestFixture]
    class ParserTestFixture
    {

        IDiagnostics diag;
        IEmit emit;
        ITable table;

        [SetUp]
        public void SetUp()
        {
            this.diag = Substitute.For<IDiagnostics>();
            this.table = new Table();
            this.emit = Substitute.For<IEmit>();
        }

        [Test]
        [TestCase("int a", true)]
        [TestCase("pchar fdsf", true)]
        [TestCase("c sdf", false)]
        public void TestIfRecognizesVarDef(string input, bool isVarDef)
        {
            IScanner scanner = new Scanner(new StringReader(input));
            Parser parser = new Parser(scanner, this.emit, this.table, this.diag);
            parser.AddPredefinedSymbols();
            parser.ReadNextToken();
            Assert.AreEqual(isVarDef, parser.IsVarOrFuncDef());
        }
    }
}
