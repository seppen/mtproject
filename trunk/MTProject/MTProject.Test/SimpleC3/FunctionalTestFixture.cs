﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

namespace MTProject.Test.SimpleC3
{
    [TestFixture]
    class FunctionalTestFixture : TestCaseRunner
    {
        public override string Version()
        {
            return "SimpleC3";
        }

        public override string Extension()
        {
            return "sc3";
        }

        [Test]
        public void SimpleProgram()
        {
            this.RunTestMethod("SimpleProgram");
        }

        [Test]
        public void SingleFunction()
        {
            this.RunTestMethod("SingleFunction");
        }

        [Test]
        public void NestedFunctions()
        {
            this.RunTestMethod("NestedFunctions");
        }

        [Test]
        public void EmbeddedFunctions()
        {
            this.RunTestMethod("EmbeddedFunctions");
        }

        [Test]
        public void FunctionWithParameter()
        {
            this.RunTestMethod("FunctionWithParameter");
        }

        [Test]
        public void ComplexFunctionsWithParameter()
        {
            this.RunTestMethod("ComplexFunctionsWithParameter");
        }

        [Test]
        public void Assignments()
        {
            this.RunTestMethod("Assignments");
        }

        [Test]
        public void Fibonacci()
        {
            this.RunTestMethod("Fibonacci");
        }

        [Test]
        public void WhileExample()
        {
            this.RunTestMethod("WhileExample");
        }

        [Test]
        public void StopStatements()
        {
            this.RunTestMethod("StopStatements");
        }
    }
}
