﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

namespace MTProject.Test.SimpleC1
{
    [TestFixture]
    class FunctionalTestFixture : TestCaseRunner
    {
        public override string Version()
        {
            return "SimpleC1";
        }

        public override string Extension()
        {
            return "sc1";
        }

        [Test]
        public void EmptyProgramTest()
        {
            this.RunTestMethod("EmptyProgram");
        }

        [Test]
        public void WritelineStatement()
        {
            this.RunTestMethod("WritelineStatement");
        }

        [Test]
        public void WritelineExpression()
        {
            this.RunTestMethod("WritelineExpression");
        }

        [Test]
        public void ComplexExample()
        {
            this.RunTestMethod("ComplexExample");
        }

        [Test]
        public void NonVoidStatement()
        {
            this.RunTestMethod("NonVoidStatement");
        }

        [Test]
        public void ErrorExample()
        {
            this.RunTestMethod("Error-Example");
        }
    }
}
