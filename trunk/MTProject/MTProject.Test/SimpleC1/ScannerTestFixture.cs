﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NUnit.Framework;

using MTProject.Core;
using MTProject.Core.Tokens;

namespace MTProject.Test.SimpleC1
{
    [TestFixture]
    class ScannerTestFixture
    {
        Scanner scanner;
        TextReader reader;

        [Test]
        [TestCase("123")]
        public void NumberTokenTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Assert.IsTrue(this.scanner.Next() is NumberToken);
        }

        [Test]
        [TestCase("xA_1", true)]
        [TestCase("1x", false)]
        public void IdentTokenTest(string input, bool isIdent)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Assert.AreEqual(this.scanner.Next() is IdentToken, isIdent);
        }

        [Test]
        [TestCase("scanf")]
        [TestCase("printf")]
        public void KeywordTokenTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Token token = this.scanner.Next();
            Assert.IsTrue(token is KeywordToken);
        }

        [Test]
        [TestCase("=")]
        [TestCase("(")]
        [TestCase(")")]
        [TestCase(";")]
        [TestCase("+")]
        [TestCase("-")]
        [TestCase("*")]
        [TestCase("|")]
        [TestCase("/")]
        [TestCase("%")]
        [TestCase("&")]
        [TestCase("~")]
        [TestCase("++")]
        [TestCase("--")]
        public void SpecialSymbolTest(string input)
        {
            this.reader = new StringReader(input);
            this.scanner = new Scanner(this.reader);
            Assert.IsTrue(this.scanner.Next() is SpecialSymbolToken);
        }
    }
}
