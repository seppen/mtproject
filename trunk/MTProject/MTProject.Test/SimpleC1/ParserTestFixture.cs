﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NUnit.Framework;

using NSubstitute;

using MTProject.Core;
using MTProject.Core.Tokens;

namespace MTProject.Test.SimpleC1
{
    /// <remarks>
    /// Scanner class has to be working.
    /// Parser.Error is mocked to do nothing to avoid not defined ident problems
    /// </remarks>
    [TestFixture]
    class ParserTestFixture
    {

        IDiagnostics diag;
        IEmit emit;
        Table table;

        [SetUp]
        public void SetUp()
        {
            this.diag = Substitute.For<IDiagnostics>();
            this.table = new Table(new List<string>());
            this.emit = Substitute.For<IEmit>();
        }

        [Test]
        public void TestIfRecognizesScanFunc()
        {
            Scanner scanner = new Scanner(new StringReader("scanf()"));
            Parser parser = Substitute.For<Parser>(scanner, this.emit, this.table, this.diag);
            parser.ReadNextToken();
            Assert.IsTrue(parser.IsScanFunc());
        }

        [Test]
        public void TestIfRecognizesPrintFunc()
        {
            Scanner scanner = new Scanner(new StringReader("printf(a)"));
            Parser parser = Substitute.For<Parser>(scanner, this.emit, this.table, this.diag);
            parser.ReadNextToken();
            Assert.IsTrue(parser.IsPrintFunc());
        }

        [Test]
        [TestCase("a", typeof(int))]
        [TestCase("a=4", typeof(void))]
        [TestCase("++a", typeof(int))]
        [TestCase("--a", typeof(int))]
        [TestCase("a--", typeof(int))]
        [TestCase("a++", typeof(int))]
        [TestCase("123", typeof(int))]
        [TestCase("~(a+0)", typeof(int))]
        [TestCase("printf(a+v)", typeof(void))]
        [TestCase("scanf()", typeof(int))]
        [TestCase("(e%3 + v*2-1&b|t)", typeof(int))]
        public void TestIfRecognizesPrimaryExpression(string input, Type expectedType)
        {
            Type type;
            Scanner scanner = new Scanner(new StringReader(input));
            Parser parser = Substitute.For<Parser>(scanner, this.emit, this.table, this.diag);
            parser.ReadNextToken();
            Assert.IsTrue(parser.IsPrimaryExpression(out type) && type == expectedType);
        }

        [Test]
        [TestCase("--a+h;", true)]
        [TestCase("a = scanf();", true)]
        [TestCase(";", true)]
        [TestCase("e+0", false)]
        public void TestIfRecognizesStatement(string input, bool isStatement)
        {
            Scanner scanner = new Scanner(new StringReader(input));
            Parser parser = Substitute.For<Parser>(scanner, this.emit, this.table, this.diag);
            parser.ReadNextToken();
            Assert.AreEqual(parser.IsStatement(), isStatement);
        }
    }
}
