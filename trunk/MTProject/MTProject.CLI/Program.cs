﻿using System;
using System.IO;
using System.Collections.Generic;

using MTProject.Core;

namespace MTProject.CLI
{
    class Program
    {
        public static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Syntax: MTProject.CLI <source file> [<result exe file>]");
                return -1;
            }

            string assemblyName;

            if (args.Length == 2)
            {
                assemblyName = args[1];
            }
            else
            {
                assemblyName = Path.ChangeExtension(args[0], "exe");
            }

            Compiler.Compile(args[0], assemblyName);

            return 0;
        }
    }
}
