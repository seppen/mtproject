﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace MTProject.Core
{
    public interface IEmit
    {
        TypeBuilder InitProgramClass(string programName);
        void WriteExecutable();
        MethodInfo StartFunction(string functionName, Type returnType, Type[] parameterTypes);
        void EndFunction();
        void AddCast(Type castType, Type exprType);
        void AddConcatenationOp();
        void AddConditionOp(string op);
        void AddAdditiveOp(string op);
        void AddMultiplicativeOp(string op);
        void AddUnaryOp(string op);
        ParameterBuilder AddParam(string paramName, int paramIndex, Type type);
        void AddGetParameter(ParameterBuilder parameterInfo);
        void AddIncParameter(ParameterBuilder parameterInfo, Parser.IncDecOps incOp);
        void AddParameterAssigment(ParameterBuilder parameterInfo);
        void AddMethodCall(MethodInfo methodInfo);
        Label GetLabel();
        void MarkLabel(Label label);
        void AddCondBranch(Label label);
        void AddBranch(Label label);
        void AddReturn();
        void AddPop();
        void BeginScope();
        void EndScope();
        LocalBuilder AddLocalVar(string localVarName, Type localVarType);
        void FinishLastMethod();
        void AddLocalVarAssigment(LocalVariableInfo localVariableInfo);
        void AddGetLocalVar(LocalVariableInfo localVariableInfo);
        void AddIncLocalVar(LocalVariableInfo localVariableInfo, Parser.IncDecOps incOp);
        void AddGetNumber(long value);
        void AddGetString(string value);
        void AddGetBoolean(bool value);
        void AddGetChar(char value);
    }
}
