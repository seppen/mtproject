using System;

namespace MTProject.Core
{
    public interface IDiagnostics
    {
        void Error(int line, int column, String message);
        void Warning(int line, int column, String message);
        void Note(int line, int column, String message);
        int GetErrorCount();
        void BeginSourceFile(string file);
        void EndSourceFile();
    }
}
