﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core
{
    public interface IScanner
    {
        Token Next();
        bool SkipComments { get; set; }
    }
}
