﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using MTProject.Core.Tokens;
using MTProject.Core.TableSymbols;

namespace MTProject.Core
{
    public interface ITable
    {
        TableSymbol Add(TableSymbol symbol);
        TableSymbol AddToUniverse(TableSymbol symbol);
        LocalVarSymbol AddLocalVar(IdentToken token, LocalBuilder localBuilder);
        FormalParamSymbol AddFormalParam(IdentToken token, Type type, ParameterBuilder parameterInfo);
        MethodSymbol AddMethod(IdentToken token, Type type, FormalParamSymbol[] formalParams, MethodInfo methodInfo);
        Dictionary<string, TableSymbol> BeginScope();
        void EndScope();
        TableSymbol GetSymbol(string ident);
        TableSymbol GetSymbol(IdentToken token);
        bool ExistCurrentScopeSymbol(string ident);
    }
}
