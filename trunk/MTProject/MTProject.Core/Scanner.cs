using System;
using System.IO;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core
{
    public class Scanner : IScanner
    {
        const char EOF = '\u001a';
        const char CR = '\r';
        const char LF = '\n';
        const char Escape = '\\';

        static readonly string keywords =
            " if else while break continue return ";
        static readonly string specialSymbols1 =
            "=();+-*|/%&~<>!";
        static readonly string specialSymbols2 =
            "+-<!>=|&*/%";
        static readonly string specialSymbols2Pairs =
            " ++ -- <= != >= == && || *= /= %= += -= ";

        TextReader reader;

        char ch;
        int line, column;

        public bool SkipComments { get; set; }

        public Scanner(TextReader reader)
        {
            this.reader = reader;
            this.line = 1;
            this.column = 0;
            this.ReadNextChar();
        }

        protected char UnEscape(char c)
        {
            switch (c)
            {
                case 't': return '\t';
                case 'n': return '\n';
                case 'r': return '\r';
                case 'f': return '\f';
                case '\'': return '\'';
                case '"': return '\"';
                case '0': return '\0';
                case Escape: return Escape;
                default: return c;
            }
        }

        protected void ReadNextChar()
        {
            int ch1 = this.reader.Read();
            this.column++;
            this.ch = (ch1 < 0) ? EOF : (char)ch1;
            if (this.ch == CR)
            {
                this.line++;
                this.column = 0;
            }
            else if (this.ch == LF)
            {
                this.column = 0;
            }
        }

        public Token Next()
        {
            int startColumn;
            int startLine;
            while (true)
            {
                startColumn = this.column;
                startLine = this.line;
                // Ident = Letter {Letter | Digit}.
                if (Alphabet.IsLetter(this.ch))
                {
                    StringBuilder s = new StringBuilder();
                    while (Alphabet.IsLetter(this.ch) || Alphabet.IsDigit(this.ch))
                    {
                        s.Append(this.ch);
                        this.ReadNextChar();
                    }
                    string id = s.ToString();
                // Boolean
                    if (id.Equals("false") || id.Equals("true"))
                    {
                        return new BooleanToken(startLine, startColumn, id.Equals("true"));
                    }
                // Keyword
                    if (keywords.Contains(" " + id + " "))
                    {
                        return new KeywordToken(startLine, startColumn, id);
                    }
                    return new IdentToken(startLine, startColumn, id);
                }
                // Number = Digit {Digit}.
                else if (Alphabet.IsDigit(this.ch))
                {
                    StringBuilder s = new StringBuilder();
                    while (Alphabet.IsDigit(this.ch))
                    {
                        s.Append(this.ch);
                        this.ReadNextChar();
                    }
                    return new NumberToken(startLine, startColumn, Convert.ToInt64(s.ToString()));
                }
                // Char
                else if (this.ch == '\'')
                {
                    this.ReadNextChar();
                    char ch1 = this.ch;
                    if (this.ch == Escape)
                    {
                        this.ReadNextChar();
                        ch1 = this.UnEscape(this.ch);
                    }
                    this.ReadNextChar();
                    if (this.ch == '\'') this.ReadNextChar();
                    return new CharToken(startLine, startColumn, ch1);
                }
                // String
                else if (this.ch == '"')
                {
                    StringBuilder s = new StringBuilder();
                    this.ReadNextChar();
                    char ch1;
                    while (this.ch != '"' && this.ch != EOF)
                    {
                        ch1 = this.ch;
                        if (this.ch == Escape)
                        {
                            ch1 = this.UnEscape(this.ch);
                        }
                        s.Append(ch1);
                        this.ReadNextChar();
                    }
                    this.ReadNextChar();
                    return new StringToken(startLine, startColumn, s.ToString());
                }
                // Comment
                else if (this.ch == '/')
                {
                    char ch1 = this.ch;
                    this.ReadNextChar();
                    if (this.ch == '/')
                    {
                        if (this.SkipComments)
                        {
                            while (this.ch != CR && this.ch != LF && this.ch != EOF)
                            {
                                this.ReadNextChar();
                            }
                            this.ReadNextChar();
                        }
                        else
                        {
                            StringBuilder s = new StringBuilder();
                            while (this.ch != CR && this.ch != LF && this.ch != EOF)
                            {
                                this.ReadNextChar();
                                s.Append(this.ch);
                            }
                            this.ReadNextChar();
                            return new CommentToken(startLine, startColumn, s.ToString(), true);
                        }
                    }
                    else if (this.ch == '*')
                    {
                        if (this.SkipComments)
                        {
                            this.ReadNextChar();
                            do
                            {
                                while (this.ch != '*' && this.ch != EOF)
                                {
                                    this.ReadNextChar();
                                }
                                this.ReadNextChar();
                            } while (this.ch != '/' && this.ch != EOF);
                            this.ReadNextChar();
                        }
                        else
                        {
                            StringBuilder s = new StringBuilder();
                            this.ReadNextChar();
                            do
                            {
                                while (this.ch != '*' && this.ch != EOF)
                                {
                                    s.Append(this.ch);
                                    this.ReadNextChar();
                                }
                                this.ReadNextChar();
                            } while (this.ch != '/' && this.ch != EOF);
                            this.ReadNextChar();
                            return new CommentToken(startLine, startColumn, s.ToString(), false);
                        }
                    }
                    // Special symbol
                    else
                    {
                        char ch2 = this.ch;
                        if (specialSymbols2Pairs.Contains(" " + ch1 + ch2 + " "))
                        {
                            this.ReadNextChar();
                            return new SpecialSymbolToken(startLine, startColumn, ch1.ToString() + ch2);
                        }
                        else if (specialSymbols1.Contains(ch1.ToString()))
                        {
                            return new SpecialSymbolToken(startLine, startColumn, ch1.ToString());
                        }
                    }
                }
                // Special symbols that may be pair
                else if (specialSymbols2.Contains(this.ch.ToString()))
                {
                    char ch1 = this.ch;
                    this.ReadNextChar();
                    char ch2 = this.ch;
                    if (specialSymbols2Pairs.Contains(" " + ch1 + ch2 + " "))
                    {
                        this.ReadNextChar();
                        return new SpecialSymbolToken(startLine, startColumn, ch1.ToString() + ch2);
                    }
                    return new SpecialSymbolToken(startLine, startColumn, ch1.ToString());
                }
                // Single Special symbol
                else if (specialSymbols1.Contains(this.ch.ToString()))
                {
                    char ch1 = this.ch;
                    this.ReadNextChar();
                    return new SpecialSymbolToken(startLine, startColumn, ch1.ToString());
                }
                // skip space
                else if (Alphabet.IsSpace(this.ch))
                {
                    this.ReadNextChar();
                    continue;
                }
                // EOF
                else if (this.ch == EOF)
                {
                    return new EOFToken(startLine, startColumn);
                }
                // Other
                else
                {
                    string s = this.ch.ToString();
                    this.ReadNextChar();
                    return new OtherToken(startLine, startColumn, s);
                }
            }
        }

        static class Alphabet
        {
            public static bool IsLetter(char ch)
            {
                return ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_';
            }

            public static bool IsDigit(char ch)
            {
                return ch >= '0' && ch <= '9';
            }

            public static bool IsSpace(char ch)
            {
                return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n';
            }
        }
    }
}
