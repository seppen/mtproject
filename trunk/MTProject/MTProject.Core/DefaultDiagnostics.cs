﻿using System;

namespace MTProject.Core
{
    public class DefaultDiagnostics : IDiagnostics
    {
        int errorCount = 0;
        public int ErrorCount
        {
            get { return this.errorCount; }
        }

        int warningCount = 0;
        public int WarningCount
        {
            get { return this.warningCount; }
        }

        int noteCount = 0;
        public int NoteCount
        {
            get { return this.noteCount; }
        }

        public void Error(int line, int column, String message)
        {
            Console.WriteLine(string.Format("Грешка на линия {0}, колона {1}: {2}", line, column, message));
            this.errorCount++;
        }

        public void Warning(int line, int column, String message)
        {
            Console.WriteLine("Внимание на линия {0}, колона {1}: {2}", line, column, message);
            this.warningCount++;
        }

        public void Note(int line, int column, String message)
        {
            Console.WriteLine("Забележка на линия {0}, колона {1}: {2}", line, column, message);
            this.noteCount++;
        }

        public int GetErrorCount()
        {
            return this.ErrorCount;
        }

        public void BeginSourceFile(string file) { }

        public void EndSourceFile() { }
    }
}
