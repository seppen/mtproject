using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

using MTProject.Core.TableSymbols;
using MTProject.Core.Tokens;

namespace MTProject.Core
{
    public class Table : ITable
    {
        Stack<Dictionary<string, TableSymbol>> symbolTable;
        Dictionary<string, TableSymbol> universeScope;

        public Table()
        {
            this.symbolTable = new Stack<Dictionary<string, TableSymbol>>();
            this.universeScope = this.BeginScope();
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            int i = this.symbolTable.Count;
            s.AppendFormat("=========\n");
            foreach (var table in this.symbolTable)
            {
                s.AppendFormat("---[{0}]---\n", i--);
                foreach (var row in table)
                {
                    s.AppendFormat("[{0}] {1}\n", row.Key, row.Value);
                }
            }
            s.AppendFormat("=========\n");
            return s.ToString();
        }

        public TableSymbol Add(TableSymbol symbol)
        {
            this.symbolTable.Peek().Add(symbol.Value, symbol);
            return symbol;
        }

        public TableSymbol AddToUniverse(TableSymbol symbol)
        {
            this.universeScope.Add(symbol.Value, symbol);
            return symbol;
        }

        public LocalVarSymbol AddLocalVar(IdentToken token, LocalBuilder localBuilder)
        {
            LocalVarSymbol result = new LocalVarSymbol(token, localBuilder);
            this.symbolTable.Peek().Add(token.Value, result);
            return result;
        }

        public FormalParamSymbol AddFormalParam(IdentToken token, Type type, ParameterBuilder parameterInfo)
        {
            FormalParamSymbol result = new FormalParamSymbol(token, type, parameterInfo);
            symbolTable.Peek().Add(token.Value, result);
            return result;
        }

        public MethodSymbol AddMethod(IdentToken token, Type type, FormalParamSymbol[] formalParams, MethodInfo methodInfo)
        {
            MethodSymbol result = new MethodSymbol(token, type, formalParams, methodInfo);
            symbolTable.Peek().Add(token.Value, result);
            return result;
        }

        public Dictionary<string, TableSymbol> BeginScope()
        {
            this.symbolTable.Push(new Dictionary<string, TableSymbol>());
            return symbolTable.Peek();
        }

        public void EndScope()
        {
            Debug.WriteLine(this.ToString());

            this.symbolTable.Pop();
        }

        public TableSymbol GetSymbol(string ident)
        {
            TableSymbol result;
            foreach (Dictionary<string, TableSymbol> table in this.symbolTable)
            {
                if (table.TryGetValue(ident, out result))
                {
                    return result;
                }
            }
            return null;
        }

        public TableSymbol GetSymbol(IdentToken token)
        {
            return this.GetSymbol(token.Value);
        }

        public bool ExistCurrentScopeSymbol(string ident)
        {
            return this.symbolTable.Peek().ContainsKey(ident);
        }
    }
}
