using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace MTProject.Core
{
    public class Emit : IEmit
    {
        AssemblyBuilder assembly;
        ModuleBuilder module;
        TypeBuilder program;
        Stack<MethodBuilder> method = new Stack<MethodBuilder>();
        Stack<ILGenerator> il = new Stack<ILGenerator>();
        string executableName;
        Stack<bool> isMain = new Stack<bool>();
        bool hasMainMethod;

        public Emit(string name)
        {
            this.executableName = name;
            AssemblyName assemblyName = new AssemblyName { Name = Path.GetFileNameWithoutExtension(name) };
            string dir = Path.GetDirectoryName(name);
            dir = String.IsNullOrWhiteSpace(dir) ? "." : dir;

            string moduleName = Path.GetFileName(name);

            this.assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Save, dir);
            this.module = this.assembly.DefineDynamicModule(assemblyName + "Module", moduleName);
            this.hasMainMethod = false;
            this.isMain.Push(false);
        }

        public TypeBuilder InitProgramClass(string programName)
        {
            this.program = this.module.DefineType(programName, TypeAttributes.Class | TypeAttributes.Public);

            return this.program;
        }

        public MethodInfo StartFunction(string functionName, Type returnType, Type[] parameterTypes)
        {
            this.isMain.Push(false);
            this.method.Push(this.program.DefineMethod(functionName, MethodAttributes.Public | MethodAttributes.Static, returnType, parameterTypes));
            this.method.Peek().InitLocals = true;
            this.il.Push(this.method.Peek().GetILGenerator());

            this.BeginScope();

            if (functionName.ToLower() == "main")
            {
                if (returnType == typeof(Int32) && parameterTypes.Length == 0)
                {
                    this.isMain.Pop();
                    this.isMain.Push(true);
                    this.hasMainMethod = true;
                    this.assembly.SetEntryPoint(this.method.Peek());
                }
            }

            return this.method.Peek();
        }

        public void EndFunction()
        {
            this.FinishLastMethod();
            this.isMain.Pop();
            this.method.Pop();
            this.il.Pop();
        }

        public void AddCast(Type castType, Type exprType)
        {
            if (!castType.IsValueType && !exprType.IsValueType)
                this.il.Peek().Emit(OpCodes.Castclass, castType);
            else if (!castType.IsValueType && exprType.IsValueType)
                this.il.Peek().Emit(OpCodes.Box, exprType);
            else if (castType.IsValueType && !exprType.IsValueType)
                this.il.Peek().Emit(OpCodes.Unbox_Any, castType);
            else
            {
                if (castType == typeof(System.Single) && exprType == typeof(System.UInt32))
                    this.il.Peek().Emit(OpCodes.Conv_R_Un);
                else if (castType == typeof(System.SByte)) // && exprType==typeof(System.Int32))
                    this.il.Peek().Emit(OpCodes.Conv_I1);
                else if (castType == typeof(System.Int16))
                    this.il.Peek().Emit(OpCodes.Conv_I2);
                else if (castType == typeof(System.Int16))
                    this.il.Peek().Emit(OpCodes.Conv_I2);
                else if (castType == typeof(System.Int32))
                    this.il.Peek().Emit(OpCodes.Conv_I4);
                else if (castType == typeof(System.Int64))
                    this.il.Peek().Emit(OpCodes.Conv_I8);
                else if (castType == typeof(System.Single))
                    this.il.Peek().Emit(OpCodes.Conv_R4);
                else if (castType == typeof(System.Double))
                    this.il.Peek().Emit(OpCodes.Conv_R8);
                else if (castType == typeof(System.Byte))
                    this.il.Peek().Emit(OpCodes.Conv_U1);
                else if (castType == typeof(System.UInt16))
                    this.il.Peek().Emit(OpCodes.Conv_U2);
                else if (castType == typeof(System.UInt32))
                    this.il.Peek().Emit(OpCodes.Conv_U4);
                else if (castType == typeof(System.UInt64))
                    this.il.Peek().Emit(OpCodes.Conv_U8);
            }
        }

        public void WriteExecutable()
        {
            this.FinishLastMethod();
            if (!this.hasMainMethod)
            {
                this.method.Push(this.program.DefineMethod("main", MethodAttributes.Static | MethodAttributes.Public, typeof(int), Type.EmptyTypes));
                this.il.Push(this.method.Peek().GetILGenerator());
                this.il.Peek().Emit(OpCodes.Ldc_I4_0);
                this.il.Peek().Emit(OpCodes.Ret);
                this.assembly.SetEntryPoint(method.Peek());
            }

            this.program.CreateType();
            this.assembly.Save(Path.GetFileName(executableName));
        }

        private static MethodInfo concatMethodInfo = typeof(String).GetMethod("Concat", new Type[] { typeof(object), typeof(object) });
        public void AddConcatenationOp()
        {
            this.il.Peek().Emit(OpCodes.Call, concatMethodInfo);
        }

        public void AddConditionOp(string op)
        {
            switch (op)
            {
                case "<":
                    this.il.Peek().Emit(OpCodes.Clt);
                    break;
                case "<=":
                    this.il.Peek().Emit(OpCodes.Cgt);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_0);
                    this.il.Peek().Emit(OpCodes.Ceq);
                    break;
                case ">":
                    this.il.Peek().Emit(OpCodes.Cgt);
                    break;
                case ">=":
                    this.il.Peek().Emit(OpCodes.Clt);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_0);
                    this.il.Peek().Emit(OpCodes.Ceq);
                    break;
                case "==":
                    this.il.Peek().Emit(OpCodes.Ceq);
                    break;
                case "!=":
                    this.il.Peek().Emit(OpCodes.Ceq);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_0);
                    this.il.Peek().Emit(OpCodes.Ceq);
                    break;
            }
        }

        public void AddAdditiveOp(string op)
        {
            switch (op)
            {
                case "+":
                    this.il.Peek().Emit(OpCodes.Add);
                    break;
                case "-":
                    this.il.Peek().Emit(OpCodes.Sub);
                    break;
                case "|":
                    this.il.Peek().Emit(OpCodes.Or);
                    break;
                case "||":
                    this.il.Peek().Emit(OpCodes.Or);
                    break;
            }
        }

        public void AddMultiplicativeOp(string op)
        {
            switch (op)
            {
                case "*":
                    this.il.Peek().Emit(OpCodes.Mul);
                    break;
                case "/":
                    this.il.Peek().Emit(OpCodes.Div);
                    break;
                case "%":
                    this.il.Peek().Emit(OpCodes.Rem);
                    break;
                case "&":
                    this.il.Peek().Emit(OpCodes.And);
                    break;
                case "&&":
                    this.il.Peek().Emit(OpCodes.And);
                    break;
            }
        }

        public void AddUnaryOp(string op)
        {
            switch (op)
            {
                case "-":
                    this.il.Peek().Emit(OpCodes.Neg);
                    break;
                case "!":
                    this.il.Peek().Emit(OpCodes.Not);
                    break;
                case "~":
                    this.il.Peek().Emit(OpCodes.Not);
                    break;
            }
        }

        public ParameterBuilder AddParam(string paramName, int paramIndex, Type type)
        {
            ParameterBuilder param = this.method.Peek().DefineParameter(paramIndex, ParameterAttributes.None, paramName);
            return param;
        }

        public void AddGetParameter(ParameterBuilder parameterInfo)
        {
            this.il.Peek().Emit(OpCodes.Ldarg, parameterInfo.Position - 1);
        }

        public void AddIncParameter(ParameterBuilder parameterInfo, Parser.IncDecOps incOp)
        {
            this.AddGetParameter(parameterInfo);

            switch (incOp)
            {
                case Parser.IncDecOps.PostInc:
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Add);
                    this.il.Peek().Emit(OpCodes.Starg, parameterInfo.Position - 1);
                    break;
                case Parser.IncDecOps.PostDec:
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Sub);
                    this.il.Peek().Emit(OpCodes.Starg, parameterInfo.Position - 1);
                    break;
                case Parser.IncDecOps.PreInc:
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Add);
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Starg, parameterInfo.Position - 1);
                    break;
                case Parser.IncDecOps.PreDec:
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Sub);
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Starg, parameterInfo.Position - 1);
                    break;
            }
        }

        public void AddParameterAssigment(ParameterBuilder parameterInfo)
        {
            this.il.Peek().Emit(OpCodes.Starg, parameterInfo.Position - 1);
        }

        public void AddMethodCall(MethodInfo methodInfo)
        {
            this.il.Peek().Emit(OpCodes.Call, methodInfo);
        }

        public Label GetLabel()
        {
            return this.il.Peek().DefineLabel();
        }

        public void MarkLabel(Label label)
        {
            this.il.Peek().MarkLabel(label);
        }

        public void AddCondBranch(Label label)
        {
            this.il.Peek().Emit(OpCodes.Brfalse, label);
        }

        public void AddBranch(Label label)
        {
            this.il.Peek().Emit(OpCodes.Br, label);
        }

        public void AddReturn()
        {
            this.il.Peek().Emit(OpCodes.Ret);
        }

        public void AddPop()
        {
            this.il.Peek().Emit(OpCodes.Pop);
        }

        public void BeginScope()
        {
            this.il.Peek().BeginScope();
        }

        public void EndScope()
        {
            this.il.Peek().EndScope();
        }

        public LocalBuilder AddLocalVar(string localVarName, Type localVarType)
        {
            LocalBuilder result = this.il.Peek().DeclareLocal(localVarType);
            return result;
        }

        public void FinishLastMethod()
        {
            if (this.method.Count == 0) return;

            if (this.isMain.Peek())
            {
                this.il.Peek().Emit(OpCodes.Ldc_I4_0);
            }

            this.il.Peek().Emit(OpCodes.Ret);

            this.EndScope();
        }

        public void AddLocalVarAssigment(LocalVariableInfo localVariableInfo)
        {
            this.il.Peek().Emit(OpCodes.Stloc, (LocalBuilder)localVariableInfo);
        }

        public void AddGetLocalVar(LocalVariableInfo localVariableInfo)
        {
            this.il.Peek().Emit(OpCodes.Ldloc, (LocalBuilder)localVariableInfo);
        }

        public void AddIncLocalVar(LocalVariableInfo localVariableInfo, Parser.IncDecOps incOp)
        {
            this.AddGetLocalVar(localVariableInfo);

            switch (incOp)
            {
                case Parser.IncDecOps.PostInc:
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Add);
                    this.il.Peek().Emit(OpCodes.Stloc, (LocalBuilder)localVariableInfo);
                    break;
                case Parser.IncDecOps.PostDec:

                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Sub);
                    this.il.Peek().Emit(OpCodes.Stloc, (LocalBuilder)localVariableInfo);
                    break;
                case Parser.IncDecOps.PreInc:
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Add);
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Stloc, (LocalBuilder)localVariableInfo);
                    break;
                case Parser.IncDecOps.PreDec:
                    this.il.Peek().Emit(OpCodes.Ldc_I4_1);
                    this.il.Peek().Emit(OpCodes.Sub);
                    this.il.Peek().Emit(OpCodes.Dup);
                    this.il.Peek().Emit(OpCodes.Stloc, (LocalBuilder)localVariableInfo);
                    break;
            }
        }

        public void AddGetNumber(long value)
        {
            if (value >= Int32.MinValue && value <= Int32.MaxValue)
            {
                this.il.Peek().Emit(OpCodes.Ldc_I4, (Int32)value);
            }
            else
            {
                this.il.Peek().Emit(OpCodes.Ldc_I8, value);
            }
        }

        public void AddGetString(string value)
        {
            this.il.Peek().Emit(OpCodes.Ldstr, value);
        }

        public void AddGetBoolean(bool value)
        {
            this.il.Peek().Emit(OpCodes.Ldc_I4, value ? 1 : 0);
        }

        public void AddGetChar(char value)
        {
            this.il.Peek().Emit(OpCodes.Ldc_I4_S, value);
        }
    }
}
