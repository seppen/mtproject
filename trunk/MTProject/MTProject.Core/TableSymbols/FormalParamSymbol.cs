using System;
using System.Reflection.Emit;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public class FormalParamSymbol : TableSymbol
    {
        public Type ParamType { get; set; }
        public ParameterBuilder ParameterInfo { get; set; }

        public FormalParamSymbol(IdentToken token, Type paramType, ParameterBuilder parameterInfo) : base(token.Line, token.Column, token.Value)
        {
            this.ParamType = paramType;
            this.ParameterInfo = parameterInfo;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3} formalparamtype={4}", this.Line, this.Column, this.Value, this.GetType(), this.ParamType);
            return s.ToString();
        }
    }
}
