using System;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public class PrimitiveTypeSymbol : TypeSymbol
    {
        public PrimitiveTypeSymbol(IdentToken token, Type type) : base(token, type) { }
    }
}
