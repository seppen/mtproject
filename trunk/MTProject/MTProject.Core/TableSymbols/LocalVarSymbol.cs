using System;
using System.Reflection;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public class LocalVarSymbol : TableSymbol
    {
        public LocalVariableInfo LocalVariableInfo { get; set; }

        public LocalVarSymbol(IdentToken token, LocalVariableInfo localVariableInfo) : base(token.Line, token.Column, token.Value)
        {
            this.LocalVariableInfo = localVariableInfo;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3} localvartype={4} localindex={5}", this.Line, this.Column, this.Value, this.GetType(), this.LocalVariableInfo.LocalType, this.LocalVariableInfo.LocalIndex);
            return s.ToString();
        }
    }
}
