using System;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public abstract class TypeSymbol : TableSymbol
    {
        public Type Type { get; set; }

        public TypeSymbol(IdentToken token, Type type) : base(token.Line, token.Column, token.Value)
        {
            this.Type = type;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3} type={4}", this.Line, this.Column, this.Value, this.GetType(), this.Type.FullName);
            return s.ToString();
        }
    }
}
