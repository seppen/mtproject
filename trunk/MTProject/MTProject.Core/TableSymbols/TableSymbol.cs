using System;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public abstract class TableSymbol : IdentToken
    {
        public TableSymbol(int line, int column, string value) : base(line, column, value) { }
    }
}
