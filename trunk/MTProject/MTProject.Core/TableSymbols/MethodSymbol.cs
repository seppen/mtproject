using System;
using System.Reflection;
using System.Text;

using MTProject.Core.Tokens;

namespace MTProject.Core.TableSymbols
{
    public class MethodSymbol : TableSymbol
    {
        public Type ReturnType { get; set; }
        public FormalParamSymbol[] FormalParams { get; set; }
        public MethodInfo MethodInfo { get; set; }

        public MethodSymbol(IdentToken token, Type returnType, FormalParamSymbol[] formalParams, MethodInfo methodInfo) : base(token.Line, token.Column, token.Value)
        {
            this.ReturnType = returnType;
            this.FormalParams = formalParams;
            this.MethodInfo = methodInfo;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3} methodsignature={4} {5}(", this.Line, this.Column, this.Value, this.GetType(), this.ReturnType, this.Value);
            foreach (FormalParamSymbol param in this.FormalParams)
            {
                s.AppendFormat("{0} {1}, ", param.ParamType, param.Value);
            }
            if (this.FormalParams.Length != 0) s.Remove(s.Length - 2, 2);
            s.Append(")");
            return s.ToString();
        }
    }
}
