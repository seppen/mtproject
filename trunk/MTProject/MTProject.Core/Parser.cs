﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

using MTProject.Core.TableSymbols;
using MTProject.Core.Tokens;

namespace MTProject.Core
{
    public class Parser
    {
        IScanner scanner;
        IEmit emit;
        ITable symbolTable;
        Token prevToken;
        Token token;
        IDiagnostics diag;
        string defaultProgramName = "Program";

        Stack<System.Reflection.Emit.Label> breakStack = new Stack<System.Reflection.Emit.Label>();
        Stack<System.Reflection.Emit.Label> continueStack = new Stack<System.Reflection.Emit.Label>();

        public Parser(IScanner scanner, IEmit emit, ITable symbolTable, IDiagnostics diag)
        {
            this.scanner = scanner;
            this.scanner.SkipComments = true;
            this.emit = emit;
            this.symbolTable = symbolTable;
            this.diag = diag;
        }

        public bool Parse()
        {
            this.ReadNextToken();
            this.AddPredefinedSymbols();
            return this.IsProgram();
        }

        public void ReadNextToken()
        {
            this.prevToken = this.token;
            this.token = this.scanner.Next();
        }

        #region Predefinitions

        public void AddPredefinedSymbols()
        {
            this.symbolTable.AddToUniverse(new PrimitiveTypeSymbol(new IdentToken(-1, -1, "int"), typeof(int)));
            this.symbolTable.AddToUniverse(new PrimitiveTypeSymbol(new IdentToken(-1, -1, "bool"), typeof(bool)));
            this.symbolTable.AddToUniverse(new PrimitiveTypeSymbol(new IdentToken(-1, -1, "char"), typeof(char)));
            this.symbolTable.AddToUniverse(new PrimitiveTypeSymbol(new IdentToken(-1, -1, "pchar"), typeof(string)));
        }

        public void AddPredefinedMethods()
        {
            // printf
            MethodSymbol printf = this.symbolTable.AddMethod(new IdentToken(-1, -1, "printf"), typeof(void), null, null);
            printf.MethodInfo = typeof(Console).GetMethod("WriteLine", new Type[] { typeof(object) });
            printf.FormalParams = new FormalParamSymbol[] { new FormalParamSymbol(new IdentToken(-1, -1, "value"), typeof(object), null) };

            // scanf
            MethodSymbol scanf_i = this.symbolTable.AddMethod(new IdentToken(-1, -1, "scanf_i"), typeof(int), null, null);
            MethodSymbol scanf_s = this.symbolTable.AddMethod(new IdentToken(-1, -1, "scanf_s"), typeof(string), null, null);
            MethodSymbol scanf_b = this.symbolTable.AddMethod(new IdentToken(-1, -1, "scanf_b"), typeof(bool), null, null);
            MethodSymbol scanf_c = this.symbolTable.AddMethod(new IdentToken(-1, -1, "scanf_c"), typeof(char), null, null);

            scanf_s.MethodInfo = typeof(Console).GetMethod("ReadLine");
            scanf_s.FormalParams = new FormalParamSymbol[] { };

            scanf_i.MethodInfo = this.emit.StartFunction("scanf_i", scanf_i.ReturnType, new Type[] { });
            this.emit.AddMethodCall(scanf_s.MethodInfo);
            this.emit.AddMethodCall(scanf_i.ReturnType.GetMethod("Parse", new Type[] { typeof(string) }));
            this.emit.EndFunction();
            scanf_i.FormalParams = new FormalParamSymbol[] { };

            scanf_b.MethodInfo = this.emit.StartFunction("scanf_b", scanf_b.ReturnType, new Type[] { });
            this.emit.AddMethodCall(scanf_s.MethodInfo);
            this.emit.AddMethodCall(scanf_b.ReturnType.GetMethod("Parse", new Type[] { typeof(string) }));
            this.emit.EndFunction();
            scanf_b.FormalParams = new FormalParamSymbol[] { };

            scanf_c.MethodInfo = this.emit.StartFunction("scanf_c", scanf_c.ReturnType, new Type[] { });
            this.emit.AddMethodCall(scanf_s.MethodInfo);
            this.emit.AddMethodCall(scanf_c.ReturnType.GetMethod("Parse", new Type[] { typeof(string) }));
            this.emit.EndFunction();
            scanf_c.FormalParams = new FormalParamSymbol[] { };

            // ord
            MethodSymbol ord = this.symbolTable.AddMethod(new IdentToken(-1, -1, "ord"), typeof(int), null, null);
            ord.MethodInfo = typeof(Convert).GetMethod("ToInt32", new Type[] { typeof(char) });
            ord.FormalParams = new FormalParamSymbol[] { new FormalParamSymbol(new IdentToken(-1, -1, "value"), typeof(char), null) };

            // chr
            MethodSymbol chr = this.symbolTable.AddMethod(new IdentToken(-1, -1, "chr"), typeof(char), null, null);
            chr.MethodInfo = typeof(Convert).GetMethod("ToChar", new Type[] { typeof(int) });
            chr.FormalParams = new FormalParamSymbol[] { new FormalParamSymbol(new IdentToken(-1, -1, "value"), typeof(int), null) };

            // abs
            MethodSymbol abs = this.symbolTable.AddMethod(new IdentToken(-1, -1, "abs"), typeof(int), null, null);
            abs.MethodInfo = typeof(Math).GetMethod("Abs", new Type[] { typeof(int) });
            abs.FormalParams = new FormalParamSymbol[] { new FormalParamSymbol(new IdentToken(-1, -1, "value"), typeof(int), null) };

            // sqr
            MethodSymbol sqr = this.symbolTable.AddMethod(new IdentToken(-1, -1, "sqr"), typeof(int), null, null);
            sqr.MethodInfo = this.emit.StartFunction("sqr", typeof(int), new Type[] { typeof(int) });
            sqr.FormalParams = new FormalParamSymbol[] { new FormalParamSymbol(new IdentToken(-1, -1, "value"), typeof(int), null) };
            sqr.FormalParams[0].ParameterInfo = this.emit.AddParam("value", 1, typeof(int));
            this.emit.AddGetParameter(sqr.FormalParams[0].ParameterInfo);
            this.emit.AddGetParameter(sqr.FormalParams[0].ParameterInfo);
            this.emit.AddMultiplicativeOp("*");
            this.emit.AddReturn();
            this.emit.EndFunction();

        }

        #endregion

        #region Grammar Methods

        // [1] Program = {Statement}.
        public bool IsProgram()
        {
            this.emit.InitProgramClass(this.defaultProgramName);
            this.AddPredefinedMethods();
            this.emit.StartFunction("main", typeof(int), new Type[] { });
            while (this.IsStatement()) ;
            this.emit.EndFunction();
            return this.diag.GetErrorCount() == 0;
        }

        // [2]  Statement = CompoundSt | IfSt | WhileSt | StopSt | [Expression] ';'.
        public bool IsStatement()
        {
            Type type = typeof(void);

            if (this.IsCompoundSt() || this.IsIfSt() || this.IsWhileSt() || this.IsStopSt()) return true;

            if (this.IsExpression(out type))
            {
                if (!this.CheckSpecialSymbol(";"))
                {
                    this.Error("Очаква се специален символ ;.");
                    return false;
                }
                if (type != typeof(void)) this.emit.AddPop();

                return true;
            }

            if (this.CheckSpecialSymbol(";")) return true;

            return false;
        }

        // [3]  CompoundSt = '{' {Declaration} {Statement} '}'
        public bool IsCompoundSt()
        {
            if (!this.CheckOtherToken("{")) return false;

            this.symbolTable.BeginScope();
            this.emit.BeginScope();

            while (this.IsDeclaration()) ;
            while (this.IsStatement()) ;

            this.emit.EndScope();
            this.symbolTable.EndScope();

            if (!this.CheckOtherToken("}"))
            {
                this.Error("Очаква се }.");
                return false;
            }

            return true;
        }

        // [4]  Declaration = VarDef | FuncDef.
        public bool IsDeclaration()
        {
            return this.IsVarOrFuncDef();
        }

        // [5]  VarDef = TypeIdent Ident.
        // [6]  FuncDef = TypeIdent Ident '(' TypeIdent Ident ')' CompoundSt.
        public bool IsVarOrFuncDef()
        {
            Type type;
            IdentToken ident;

            if (!this.CheckTypeIdent(out type)) return false;
            if (!this.CheckIdent()) return false;

            ident = (IdentToken)this.prevToken;

            if (this.symbolTable.ExistCurrentScopeSymbol(ident.Value))
            {
                this.Error("Символът " + ident.Value + " вече е дефиниран.");
                return false;
            }

            // FuncDef
            if (this.CheckSpecialSymbol("("))
            {
                Type parameterType;
                if (!this.CheckTypeIdent(out parameterType)) this.Error("Очаква се typeident.");
                IdentToken parameterIdent;
                if (!this.CheckIdent()) this.Error("Очаква се ident.");

                parameterIdent = (IdentToken)this.prevToken;

                if (!this.CheckSpecialSymbol(")")) this.Error("Очаква се ).");

                MethodSymbol method = this.symbolTable.AddMethod(ident, type, null, null);

                this.symbolTable.BeginScope();

                FormalParamSymbol param = this.symbolTable.AddFormalParam(parameterIdent, parameterType, null);

                method.MethodInfo = this.emit.StartFunction(ident.Value, type, new Type[] { parameterType });
                param.ParameterInfo = this.emit.AddParam(parameterIdent.Value, 1, parameterType);

                method.FormalParams = new FormalParamSymbol[] { param };

                if (!this.IsCompoundSt()) this.Error("Очаква се compound_statement.");

                this.symbolTable.EndScope();
                this.emit.EndFunction();
            }
            // VarDef
            else
            {
                this.symbolTable.AddLocalVar(ident, this.emit.AddLocalVar(ident.Value, type));
            }

            if (this.CheckSpecialSymbol(";"))
            {
                Console.WriteLine("Skipped a ; in declaration");
            }

            return true;
        }

        // [7]  IfSt = 'if' '(' Expression ')' Statement ['else' Statement].
        public bool IsIfSt()
        {
            Type conditionType;
            if (!this.CheckKeyword("if")) return false;
            if (!this.CheckSpecialSymbol("(")) this.Error("Очаква се (.");
            if (!this.IsExpression(out conditionType)) this.Error("Очаква се expr.");
            if (typeof(bool) != conditionType) this.Error("Изразът трябва да е bool.");
            if (!this.CheckSpecialSymbol(")")) this.Error("Очаква се ).");

            System.Reflection.Emit.Label elseLabel = this.emit.GetLabel();
            this.emit.AddCondBranch(elseLabel);

            if (!this.IsStatement()) this.Error("Очаква се statement.");

            if (this.CheckKeyword("else"))
            {
                System.Reflection.Emit.Label endLabel = this.emit.GetLabel();
                this.emit.AddBranch(endLabel);
                this.emit.MarkLabel(elseLabel);
                if (!this.IsStatement()) this.Error("Очаква се statement.");
                this.emit.MarkLabel(endLabel);
            }
            else
            {
                this.emit.MarkLabel(elseLabel);
            }

            return true;
        }

        // [8]  WhileSt = 'while' '(' Expression ')' Statement.
        public bool IsWhileSt()
        {
            if (!this.CheckKeyword("while")) return false;

            System.Reflection.Emit.Label continueLabel = this.emit.GetLabel();
            System.Reflection.Emit.Label breakLabel = this.emit.GetLabel();
            this.breakStack.Push(breakLabel);
            this.continueStack.Push(continueLabel);

            this.emit.MarkLabel(continueLabel);
            Type conditionType;
            if (!this.CheckSpecialSymbol("(")) this.Error("Очаква се (.");
            if (!this.IsExpression(out conditionType)) this.Error("Очаква се expr.");
            if (typeof(bool) != conditionType) this.Error("Изразът трябва да е bool.");
            if (!this.CheckSpecialSymbol(")")) this.Error("Очаква се ).");
            this.emit.AddCondBranch(breakLabel);

            if (!this.IsStatement()) this.Error("Очаква се statement.");

            this.emit.AddBranch(continueLabel);
            this.emit.MarkLabel(breakLabel);

            this.breakStack.Pop();
            this.continueStack.Pop();

            return true;
        }

        // [9]  StopSt = 'break' ';' | 'continue' ';' | 'return' [Expression] ';'
        public bool IsStopSt()
        {
            if (this.CheckKeyword("return"))
            {
                Type type;
                if (this.IsExpression(out type)) this.emit.AddReturn();
                if (!this.CheckSpecialSymbol(";")) this.Error("Очаква се ;.");
                return true;
            }

            if (this.CheckKeyword("break"))
            {
                if (!this.CheckSpecialSymbol(";")) this.Error("Очаква се ;.");
                if (this.breakStack.Count != 0)
                {
                    this.emit.AddBranch(this.breakStack.Peek());
                }
                else
                {
                    this.Error("Не можете да използвате break тук.");
                }
                return true;
            }

            if (this.CheckKeyword("continue"))
            {
                if (!this.CheckSpecialSymbol(";")) this.Error("Очаква се ;.");
                if (this.continueStack.Count != 0)
                {
                    this.emit.AddBranch(this.continueStack.Peek());
                }
                else
                {
                    this.Error("Не можете да използвате continue тук.");
                }
                return true;
            }

            return false;
        }

        // [10] Expression =  AdditiveExpr [('<' | '<=' | '==' | '!=' | '>=' | '>') AdditiveExpr].
        public bool IsExpression(out Type type)
        {
            if (!this.IsAdditiveExpression(out type)) return false;

            SpecialSymbolToken sym;
            Type termType;

            if (this.CheckSpecialSymbol("<") || this.CheckSpecialSymbol("<=") || this.CheckSpecialSymbol("==") ||
                this.CheckSpecialSymbol("!=") || this.CheckSpecialSymbol(">=") || this.CheckSpecialSymbol(">"))
            {
                sym = (SpecialSymbolToken)this.prevToken;

                if (!this.IsAdditiveExpression(out termType)) this.Error("Очаква се additive_expression.");

                if (type != termType) this.Error("Несъвместими типове.");

                type = typeof(bool);
                this.emit.AddConditionOp(sym.Value);
            }

            return true;
        }

        // [11] AdditiveExpr = ['+' | '-'] MultiplicativeExpr {('+' | '-' | '|' | '||') MultiplicativeExpr}.
        public bool IsAdditiveExpression(out Type type)
        {
            SpecialSymbolToken unary = null;

            if (this.CheckSpecialSymbol("+") || this.CheckSpecialSymbol("-")) unary = (SpecialSymbolToken)this.prevToken;

            if (!this.IsMultiplicativeExpression(out type))
            {
                if (unary != null) this.Error("Очаква се multiplicative_expression.");
                return false;
            }

            if (unary != null && unary.Value == "-") this.emit.AddUnaryOp(unary.Value);

            SpecialSymbolToken t;
            Type termType;

            while (this.CheckSpecialSymbol("+") || this.CheckSpecialSymbol("-") || this.CheckSpecialSymbol("|") || this.CheckSpecialSymbol("||"))
            {
                t = (SpecialSymbolToken)this.prevToken;

                if (!this.IsMultiplicativeExpression(out termType)) this.Error("Очаква се multiplicative_expression.");

                if (type != termType) this.Error("Несъвместими типове.");

                if (t.Value == "+" && type == typeof(string))
                {
                    this.emit.AddConcatenationOp();
                }
                else
                {
                    this.emit.AddAdditiveOp(t.Value);
                }
            }

            return true;
        }

        // [12] MultiplicativeExpr = SimpleExpr {('*' | '/' | '%' | '&' | '&&') SimpleExpr}.
        public bool IsMultiplicativeExpression(out Type type)
        {
            if (!this.IsSimpleExpression(out type)) return false;

            SpecialSymbolToken sym;
            Type termType;

            while (this.CheckSpecialSymbol("*") || this.CheckSpecialSymbol("/") || this.CheckSpecialSymbol("%") ||
                   this.CheckSpecialSymbol("&") || this.CheckSpecialSymbol("&&"))
            {
                sym = (SpecialSymbolToken)this.prevToken;

                if (!this.IsSimpleExpression(out termType)) this.Error("Очаква се primary_expression.");

                if (type != termType) this.Error("Несъвместими типове.");

                this.emit.AddMultiplicativeOp(sym.Value);
            }

            return true;
        }

        public bool IsSimpleExpression(out Type type)
        {
            return this.IsPrimaryExpression(out type);
        }

        // [13] SimpleExpr = ('++' | '--' | '-' | '~' | '!') PrimaryExpr | PrimaryExpr ['++' | '--'].
        // [14] PrimaryExpr = Constant | Variable | VarIdent [('='|'+='|'-='|'*='|'/='|'%=') Expression] |
		//			'*' VarIdent | '&' VarIdent | FuncIdent '(' [Expression] ')' | '(' Expression ')'.
        // [16] Variable = ['+'|'-'] VarIdent.
        public enum IncDecOps { None, PreInc, PreDec, PostInc, PostDec }
        public bool IsPrimaryExpression(out Type type)
        {
            // Constant
            if (this.IsConstant(out type)) return true;

            if (this.CheckSpecialSymbol("~") || this.CheckSpecialSymbol("!") || this.CheckSpecialSymbol("-"))
            {
                SpecialSymbolToken op = (SpecialSymbolToken)this.prevToken;
                if (this.IsPrimaryExpression(out type))
                {
                    this.emit.AddUnaryOp(op.Value);
                    return true;
                }
                this.Error("Очаква се primary_expression.");
                return false;
            }

            bool unaryPlus = false;
            if (this.CheckSpecialSymbol("+")) unaryPlus = true;

            // VarIdent '++' | VarIdent '--'
            // VarIdent [('='|'+='|'-='|'*='|'/='|'%=') Expression]
            // Variable
            if (this.CheckVarIdent(out type))
            {
                IdentToken ident = (IdentToken)this.prevToken;
                TableSymbol vSymbol = this.symbolTable.GetSymbol(ident);
                Type vType = type;

                if (this.CheckSpecialSymbol("--") || this.CheckSpecialSymbol("++"))
                {
                    IncDecOps op = (((SpecialSymbolToken)this.prevToken).Value == "--") ? IncDecOps.PostDec : IncDecOps.PostInc;
                    if (vSymbol == null) this.Error("Недефиниран символ " + ident.Value + "");
                    this.EmitIncVarHelper(vSymbol, op);

                    return true;
                }

                if (this.CheckSpecialSymbol("=") || this.CheckSpecialSymbol("/=") || this.CheckSpecialSymbol("-=") ||
                    this.CheckSpecialSymbol("%=") || this.CheckSpecialSymbol("*=") || this.CheckSpecialSymbol("+="))
                {
                    Type expType;
                    SpecialSymbolToken t = (SpecialSymbolToken)this.prevToken;

                    if (t.Value != "=") this.EmitGetVarHelper(vSymbol);

                    if (this.IsExpression(out expType))
                    {
                        if (vType != expType) this.Error("Несъвместими типове.");

                        if (t.Value != "=")
                        {
                            if (vType == typeof(string))
                            {
                                if ("+" == t.Value[0].ToString())
                                {
                                    this.emit.AddConcatenationOp();
                                }
                            }
                            else if ("+ -".Contains(t.Value[0].ToString()))
                            {
                                this.emit.AddAdditiveOp(t.Value[0].ToString());
                            }
                            else
                            {
                                this.emit.AddMultiplicativeOp(t.Value[0].ToString());
                            }
                        }

                        this.EmitVarAssignmentHelper(vSymbol);

                        type = typeof(void);
                        return true;
                    }
                    this.Error("Очаква се израз.");
                    return false;
                }

                this.EmitGetVarHelper(vSymbol);

                return true;

            }
            else if (unaryPlus)
            {
                this.Error("Очаква се променлива");
                return false;
            }

            // '++' VarIdent | '--' VarIdent
            if (this.CheckSpecialSymbol("++") || this.CheckSpecialSymbol("--"))
            {
                IncDecOps op = (((SpecialSymbolToken)this.prevToken).Value == "--") ? IncDecOps.PreDec : IncDecOps.PreInc;

                if (this.CheckVarIdent(out type))
                {
                    IdentToken ident = (IdentToken)this.prevToken;
                    TableSymbol vSymbol = this.symbolTable.GetSymbol(ident);

                    if (vSymbol == null) this.Error("Недефиниран символ " + ident.Value + "");

                    this.EmitIncVarHelper(vSymbol, op);

                    return true;
                }
                return false;
            }

            MethodSymbol method;
            if (this.CheckFuncIdent(out method))
            {
                if (!this.CheckSpecialSymbol("(")) this.Error("Очаква се (.");

                if (method.FormalParams.Length > 0)
                {
                    Type expType;
                    if (!this.IsExpression(out expType)) this.Error("Очаква се израз.");
                    if (!method.FormalParams[0].ParamType.IsAssignableFrom(expType)) this.Error("Несъвместими типове.");
                    this.emit.AddCast(method.FormalParams[0].ParamType, expType);
                }

                if (!this.CheckSpecialSymbol(")")) this.Error("Очаква се ).");

                this.emit.AddMethodCall(method.MethodInfo);

                type = method.ReturnType;
                return true;
            }

            if (this.CheckSpecialSymbol("(") && this.IsExpression(out type) && this.CheckSpecialSymbol(")")) return true;

            return false;
        }

        // [15] Constant = ['+'|'-'] (Number | ConstIdent) | String.
        public bool IsConstant(out Type type)
        {
            type = typeof(void);

            if (this.CheckString())
            {
                type = typeof(string);
                this.emit.AddGetString(((StringToken)this.prevToken).Value);
                return true;
            }

            if (this.CheckChar())
            {
                type = typeof(char);
                this.emit.AddGetChar(((CharToken)this.prevToken).Value);
                return true;
            }

            if (this.CheckBoolean())
            {
                type = typeof(bool);
                this.emit.AddGetBoolean(((BooleanToken)this.prevToken).Value);
                return true;
            }

            bool unaryMinus = false;
            if (this.CheckSpecialSymbol("+") || this.CheckSpecialSymbol("-"))
            {
                unaryMinus = ((SpecialSymbolToken)this.prevToken).Value == "-";
            }

            if (this.CheckNumber())
            {
                type = typeof(int);
                this.emit.AddGetNumber(((NumberToken)this.prevToken).Value);
                if (unaryMinus) this.emit.AddUnaryOp("-");
                return true;
            }

            return false;
        }

        #endregion

        #region Emit Helpers

        protected void EmitVarAssignmentHelper(TableSymbol symbol)
        {
            LocalVarSymbol local = symbol as LocalVarSymbol;
            FormalParamSymbol param = symbol as FormalParamSymbol;

            if (local != null)
            {
                this.emit.AddLocalVarAssigment(local.LocalVariableInfo);
            }
            else if (param != null)
            {
                this.emit.AddParameterAssigment(param.ParameterInfo);
            }
        }

        protected void EmitIncVarHelper(TableSymbol symbol, IncDecOps op)
        {
            LocalVarSymbol local = symbol as LocalVarSymbol;
            FormalParamSymbol param = symbol as FormalParamSymbol;

            if (local != null)
            {
                this.emit.AddIncLocalVar(local.LocalVariableInfo, op);
            }
            else if (param != null)
            {
                this.emit.AddIncParameter(param.ParameterInfo, op);
            }
        }

        protected void EmitGetVarHelper(TableSymbol symbol)
        {
            LocalVarSymbol local = symbol as LocalVarSymbol;
            FormalParamSymbol param = symbol as FormalParamSymbol;

            if (local != null)
            {
                this.emit.AddGetLocalVar(local.LocalVariableInfo);
            }
            else if (param != null)
            {
                this.emit.AddGetParameter(param.ParameterInfo);
            }
        }

        #endregion

        #region Check Methods

        public bool CheckKeyword(string keyword)
        {
            bool result = (this.token is KeywordToken) && ((KeywordToken)this.token).Value == keyword;
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckSpecialSymbol(string symbol)
        {
            bool result = (this.token is SpecialSymbolToken) && ((SpecialSymbolToken)this.token).Value == symbol;
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckOtherToken(string value)
        {
            bool result = (this.token is OtherToken) && ((OtherToken)this.token).Value == value;
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckIdent()
        {
            bool result = (this.token is IdentToken);
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckFuncIdent(out MethodSymbol symbol)
        {
            if (this.token is IdentToken)
            {
                symbol = this.symbolTable.GetSymbol((IdentToken)this.token) as MethodSymbol;
                if (symbol != null)
                {
                    this.ReadNextToken();
                    return true;
                }
            }
            symbol = null;
            return false;
        }

        public bool CheckTypeIdent(out Type type)
        {
            TypeSymbol symbol = null;
            if (this.token is IdentToken)
            {
                symbol = this.symbolTable.GetSymbol((IdentToken)this.token) as TypeSymbol;
                if (symbol != null)
                {
                    type = symbol.Type;
                    this.ReadNextToken();
                    return true;
                }
            }
            type = null;
            return false;
        }

        public bool CheckVarIdent(out Type type)
        {
            if (this.token is IdentToken)
            {
                TableSymbol symbol = this.symbolTable.GetSymbol((IdentToken)this.token);
                if (symbol != null && symbol is LocalVarSymbol)
                {
                    type = ((LocalVarSymbol)symbol).LocalVariableInfo.LocalType;
                    this.ReadNextToken();
                    return true;
                }
                else if (symbol != null && symbol is FormalParamSymbol)
                {
                    type = ((FormalParamSymbol)symbol).ParamType;
                    this.ReadNextToken();
                    return true;
                }
            }
            type = null;
            return false;
        }

        public bool CheckNumber()
        {
            bool result = (this.token is NumberToken);
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckString()
        {
            bool result = (this.token is StringToken);
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckBoolean()
        {
            bool result = (this.token is BooleanToken);
            if (result) this.ReadNextToken();
            return result;
        }

        public bool CheckChar()
        {
            bool result = (this.token is CharToken);
            if (result) this.ReadNextToken();
            return result;
        }

        #endregion

        #region Error

        protected void SkipUntilSemiColon()
        {
            Token tok = this.token;
            while (!((tok is EOFToken) ||
                       (tok is SpecialSymbolToken) && ((tok as SpecialSymbolToken).Value == ";")))
            {
                tok = this.scanner.Next();
            }
        }

        public virtual void Error(string message)
        {
            this.diag.Error(this.token.Line, this.token.Column, message);
            this.SkipUntilSemiColon();
        }

        #endregion
    }
}
