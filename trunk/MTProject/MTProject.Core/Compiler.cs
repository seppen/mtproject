using System;
using System.Collections.Generic;
using System.IO;

namespace MTProject.Core
{
    public static class Compiler
    {

        public static bool Compile(string file, string assemblyName)
        {
            return Compile(file, assemblyName, new DefaultDiagnostics());
        }

        public static bool Compile(string file, string assemblyName, IDiagnostics diag)
        {
            TextReader reader = new StreamReader(file);
            IScanner scanner = new Scanner(reader);
            ITable symbolTable = new Table();
            IEmit emit = new Emit(assemblyName);
            Parser parser = new Parser(scanner, emit, symbolTable, diag);

            diag.BeginSourceFile(file);

            if (!parser.Parse()) return false;

            diag.EndSourceFile();

            emit.WriteExecutable();

            return true;
        }
    }
}
