
namespace MTProject.Core.Tokens
{
    public abstract class Token
    {
        public int Line { get; set; }
        public int Column { get; set; }

        public Token(int line, int column)
        {
            this.Line = line;
            this.Column = column;
        }
    }
}
