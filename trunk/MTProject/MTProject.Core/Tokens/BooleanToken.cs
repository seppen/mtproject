using System.Text;

namespace MTProject.Core.Tokens
{
    public class BooleanToken : LiteralToken
    {
        public bool Value { get; set; }

        public BooleanToken(int line, int column, bool value) : base(line, column)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3}", this.Line, this.Column, this.Value, this.GetType());
            return s.ToString();
        }
    }
}
