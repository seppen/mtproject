
namespace MTProject.Core.Tokens
{
    public class EOFToken : Token
    {
        public EOFToken(int line, int column) : base(line, column) { }
    }
}
