using System.Text;

namespace MTProject.Core.Tokens
{
    public class CommentToken : Token
    {

        public string Value { get; set; }
        public bool IsInline { get; set; }

        public CommentToken(int line, int column, string value, bool isInline) : base(line, column)
        {
            this.Value = value;
            this.IsInline = isInline;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.AppendFormat("line {0}, column {1}: {2} - {3}", this.Line, this.Column, this.Value, this.GetType());
            return s.ToString();
        }
    }
}
